package com.peakbw.ugvouchers;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.peakbw.ugvouchers.com.peakbw.ugvouchers.db.VoucherContract;
import com.peakbw.ugvouchers.com.peakbw.ugvouchers.db.VoucherDBHelper;


public class Details extends Activity {

    private TextView msisdnView,dNumberView,vNumberView,tsView,latView,lonView,satelitesView,accuracyView,statusView;
    private VoucherDBHelper rDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        msisdnView = (TextView) findViewById(R.id.msisdn);
        dNumberView = (TextView) findViewById(R.id.dNumber);
        vNumberView = (TextView) findViewById(R.id.vNumber);
        tsView = (TextView) findViewById(R.id.ts);
        latView = (TextView) findViewById(R.id.lat);
        lonView = (TextView) findViewById(R.id.lon);
        satelitesView = (TextView) findViewById(R.id.satelite);
        accuracyView = (TextView) findViewById(R.id.acc);
        statusView = (TextView) findViewById(R.id.status);

        rDbHelper = new VoucherDBHelper(getBaseContext());

    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = getIntent();
        String selectedHist = intent.getStringExtra("selected");

        try{
            String [] fields = selectedHist.split("->");

            Cursor cursor = rDbHelper.getDetails(fields[1],fields[0]);

            cursor.moveToFirst();

            String msisdn = cursor.getString(cursor.getColumnIndexOrThrow(VoucherContract.History.MSISDN));
            String dNumber = cursor.getString(cursor.getColumnIndexOrThrow(VoucherContract.History.DELIVERY_NUMBER));
            String vNumber = cursor.getString(cursor.getColumnIndexOrThrow(VoucherContract.History.VOUCHER_NUMBER));
            String ts = cursor.getString(cursor.getColumnIndexOrThrow(VoucherContract.History.TS));
            String lat = cursor.getString(cursor.getColumnIndexOrThrow(VoucherContract.History.GPS_LAT));
            String lon = cursor.getString(cursor.getColumnIndexOrThrow(VoucherContract.History.GPS_LONG));
            String satelites = cursor.getString(cursor.getColumnIndexOrThrow(VoucherContract.History.NUMBER_OF_SATELITES));
            String accuracy = cursor.getString(cursor.getColumnIndexOrThrow(VoucherContract.History.ACCURACY));
            String status = cursor.getString(cursor.getColumnIndexOrThrow(VoucherContract.History.STATUS));

            msisdnView.setText(msisdn);
            dNumberView.setText(dNumber);
            vNumberView.setText(vNumber);
            tsView.setText(ts);
            latView.setText(lat);
            lonView.setText(lon);
            satelitesView.setText(satelites);
            accuracyView.setText(accuracy);
            statusView.setText(status);

            cursor.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }
}
