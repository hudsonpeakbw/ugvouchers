package com.peakbw.ugvouchers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {

    private EditText pinTextField;
    private String phoneIMEI,pin;
    private static final String ACTIVITY_ID = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pinTextField = (EditText)findViewById(R.id.pin);

        pinTextField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    validate();
                    return true;
                }
                return false;
            }
        });

        //get phone unique identifier
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        phoneIMEI = telephonyManager.getDeviceId();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void actionListener(View view){
        int viewId = view.getId();

        if(viewId == R.id.login){
            validate();
        }else if(viewId == R.id.exit){
            finish();
        }
    }

    private void validate(){
        pin = pinTextField.getText().toString();
        pinTextField.setText("");

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(pin)) {
            pinTextField.setError(getString(R.string.error_pin_required));
            focusView = pinTextField;
            cancel = true;
        } else if (pin.length() < 4) {
            pinTextField.setError(getString(R.string.error_invalid_pin));
            focusView = pinTextField;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt continue and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            startDeliveryActivity();
        }
    }

    private void startDeliveryActivity(){
        Intent di = new Intent(this,DeliveryActivity.class);
        di.putExtra("pin",pin);
        di.putExtra("imei",phoneIMEI);
        startActivity(di);

        finish();
    }

}
