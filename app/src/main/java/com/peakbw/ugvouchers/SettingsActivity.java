package com.peakbw.ugvouchers;

import android.app.Activity;
import android.os.Bundle;

import com.peakbw.ugvouchers.com.peakbw.ugvouchers.SettingsFragment;

public class SettingsActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        // Display the fragment as the main content.
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}