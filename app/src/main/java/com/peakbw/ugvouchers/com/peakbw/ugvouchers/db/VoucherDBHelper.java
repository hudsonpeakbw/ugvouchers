package com.peakbw.ugvouchers.com.peakbw.ugvouchers.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by root on 5/6/15.
 */

public class VoucherDBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "vocherDatabase.db";
    public static SQLiteDatabase mDB,rdb;
    public static final String DEBUG_TAG = "VoucherDBHelper";
    private static final String TEXT_TYPE = " TEXT";
    private static final String DATE = " DATE";
    private static final String TIMESTAMP = " TIMESTAMP";
    private static final String COMMA_SEP = ",";


    private static final String SQL_CREATE_HISTORY =
            "CREATE TABLE " + VoucherContract.History.TABLE_NAME + " (" +
                    VoucherContract.History._ID + " INTEGER PRIMARY KEY," +
                    VoucherContract.History.MSISDN + " INTEGER," +
                    VoucherContract.History.DELIVERY_NUMBER + " UNIQUE NOT NULL,"+
                    VoucherContract.History.VOUCHER_NUMBER + TEXT_TYPE + " UNIQUE NOT NULL,"+
                    VoucherContract.History.VOUCHER_PIC + TEXT_TYPE + ", "+
                    VoucherContract.History.SERIAL_PIC + TEXT_TYPE + ", "+
                    VoucherContract.History.ITEM_PIC + TEXT_TYPE + ", "+
                    VoucherContract.History.TS + TIMESTAMP + COMMA_SEP +
                    VoucherContract.History.GPS_LAT + " DOUBLE, " +
                    VoucherContract.History.GPS_LONG + " DOUBLE, " +
                    VoucherContract.History.NUMBER_OF_SATELITES + " INTEGER," +
                    VoucherContract.History.STATUS + TEXT_TYPE+ COMMA_SEP +
                    VoucherContract.History.ACCURACY  + " INTEGER " + ")";


    private static final String SQL_DELETE_HISTORY ="DROP TABLE IF EXISTS " + VoucherContract.History.TABLE_NAME;

    private static final String SQL_CREATE_PICS =
            "CREATE TABLE " + VoucherContract.Pics.TABLE_NAME + " (" +
                    VoucherContract.Pics._ID + " INTEGER PRIMARY KEY," +
                    VoucherContract.Pics.NAME + TEXT_TYPE + ", "+
                    VoucherContract.Pics.MSISDN + " INTEGER," +
                    VoucherContract.Pics.SYNCED + " BOOLEAN "+")";

    private static final String SQL_DELETE_PICS ="DROP TABLE IF EXISTS " + VoucherContract.Pics.TABLE_NAME;


    public VoucherDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        VoucherDBHelper.mDB = getWritableDatabase();
        VoucherDBHelper.rdb = getReadableDatabase();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //String pin = MainActivity.pin;
        db.execSQL(SQL_CREATE_HISTORY);
        Log.d(DEBUG_TAG, "SQL for History:" + SQL_CREATE_HISTORY);

        db.execSQL(SQL_CREATE_PICS);
        Log.d(DEBUG_TAG, "SQL for PICS:" + SQL_CREATE_PICS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {
        db.execSQL(SQL_DELETE_HISTORY);
        db.execSQL(SQL_DELETE_PICS);
        onCreate(db);
    }

    public Cursor getHistory(){
        Cursor c = rdb.rawQuery("SELECT * FROM history;", null);
        return c;
    }

    public Cursor getDetails(String msisdn,String ts){
        Cursor c = rdb.rawQuery("SELECT * FROM history WHERE ts = '"+ts+"' AND msisdn = '"+msisdn+"';", null);
        return c;
    }

    public  Cursor getPics(){
        Cursor cursor = rdb.rawQuery("SELECT name FROM pics WHERE synced = 0;", null);
        return  cursor;
    }

    
}
