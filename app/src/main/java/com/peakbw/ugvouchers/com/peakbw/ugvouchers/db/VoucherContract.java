package com.peakbw.ugvouchers.com.peakbw.ugvouchers.db;

import android.provider.BaseColumns;

/**
 * Created by root on 5/6/15.
 */
public class VoucherContract {

    public VoucherContract() {
    }

/* Inner classes that define the table contents */

    public static abstract class History implements BaseColumns {
        public static final String TABLE_NAME = "history";
        public static final String MSISDN = "msisdn";
        public static final String DELIVERY_NUMBER = "deliveryNumber";
        public static final String VOUCHER_NUMBER = "voucherNumber";

        public static final String SERIAL_PIC = "serialPic";
        public static final String ITEM_PIC = "itemPic";
        public static final String VOUCHER_PIC = "voucherPic";

        public static final String TS = "ts";
        public static final String GPS_LAT = "gpsLat";
        public static final String GPS_LONG = "gpsLong";
        public static final String NUMBER_OF_SATELITES = "sateliteNumber";
        public static final String ACCURACY = "accuracy";
        public static final String STATUS = "status";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }

    public static abstract class Pics implements BaseColumns {
        public static final String TABLE_NAME = "pics";
        public static final String NAME = "name";
        public static final String MSISDN = "msisdn";
        public static final String SYNCED = "synced";
        public static final String COLUMN_NAME_NULLABLE = "null";
    }

}
