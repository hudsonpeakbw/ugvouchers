package com.peakbw.ugvouchers.com.peakbw.ugvouchers.camera;

import java.io.File;

public abstract class AlbumStorageDirFactory {
	public abstract File getAlbumStorageDir(String albumName);
}
