package com.peakbw.ugvouchers.com.peakbw.ugvouchers.net;

/**
 * Created by root on 5/5/15.
 */

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import android.util.Log;

public class ManageServerConnections {
    private static final String DEBUG_TAG = "ManageServerConnections";
    private String xmlstr;
    public static int respCode;
    public static String timeout;

    public void setUpdateVariable(String request){
        respCode = 0;
        timeout = null;
        this.xmlstr = request;
    }

    public InputStream downloadUrl(String urlString) throws IOException {
        Log.d(DEBUG_TAG, "URL = "+urlString);
        InputStream stream = null;
        try{
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(150000 /* milliseconds */);
            conn.setConnectTimeout(20000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            //conn.setChunkedStreamingMode(0);
            OutputStream os = new BufferedOutputStream(conn.getOutputStream());
            Log.d(DEBUG_TAG, xmlstr);
            os.write(xmlstr.getBytes());
            //os.flush();
            os.close();
            conn.connect();

            Log.d(DEBUG_TAG, "Connection open");
            respCode = conn.getResponseCode();
            Log.d(DEBUG_TAG, "RespCode = "+ respCode);
            if(respCode==200){
                //MainActivity.lenghtOfFile = conn.getContentLength();
                stream = new BufferedInputStream(conn.getInputStream());
                Log.d(DEBUG_TAG, "got inputstream");
                Log.d(DEBUG_TAG, String.valueOf(conn.getResponseCode()));
            }
        }
        catch(SocketTimeoutException ex){
            timeout = "Connection Timeout!";
        }
        return stream;
    }
}

