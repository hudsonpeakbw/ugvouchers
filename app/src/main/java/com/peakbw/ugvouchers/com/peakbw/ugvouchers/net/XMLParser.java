package com.peakbw.ugvouchers.com.peakbw.ugvouchers.net;

/**
 * Created by root on 5/5/15.
 */
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.Xml;

public class XMLParser {

    private static final String DEBUG_TAG = "XMLParser";
    private static final String ns = null;
    private static String date;
    public static String message = new String(),status;
    private static ContentValues values = new ContentValues();
    private static SQLiteDatabase db;

    public static String parseXML(InputStream is){//throws XmlPullParserException, IOException

        try{


			/*int ch ;
	        StringBuffer sb = new StringBuffer();
	        while((ch = is.read())!=-1){
	            sb.append((char)ch);
	        }
	        Log.d(DEBUG_TAG, "Reply XML= "+sb.toString());*/

            status = null;


            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(is, null);
            parser.nextTag();
            parser.require(XmlPullParser.START_TAG, ns, "pbtReply");

            while (parser.next() != XmlPullParser.END_TAG) {
                if (parser.getEventType() != XmlPullParser.START_TAG) {
                    continue;
                }
                String name = parser.getName();
                Log.d(DEBUG_TAG, "TagName: "+name);

                if(name.equals("signature")){
                    parser.require(XmlPullParser.START_TAG, ns, "message");
                    if (parser.next() == XmlPullParser.TEXT) {
                        message = parser.getText();
                        //update signature
                        parser.nextTag();
                    }
                    parser.require(XmlPullParser.END_TAG, ns, "message");
                    Log.d(DEBUG_TAG, "message: "+message);
                }
                else if(name.equals("status")){
                    parser.require(XmlPullParser.START_TAG, ns, "status");
                    if (parser.next() == XmlPullParser.TEXT) {
                        status = parser.getText();
                        //update signature
                        parser.nextTag();
                    }
                    parser.require(XmlPullParser.END_TAG, ns, "status");
                    Log.d(DEBUG_TAG, "Status: "+status);
                }
                else {
                    skip(parser);
                }
            }

            if(status.trim().equalsIgnoreCase("SUCCESSFUL")){
                //
            }
        }
        catch(IOException ex){
            ex.printStackTrace();
        }
        catch(XmlPullParserException ex){
            ex.printStackTrace();
        }
        finally {
            try{
                is.close();
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return status.trim();

    }

    private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
