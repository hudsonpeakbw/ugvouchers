package com.peakbw.ugvouchers.com.peakbw.ugvouchers.services;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import com.peakbw.ugvouchers.com.peakbw.ugvouchers.db.VoucherContract;
import com.peakbw.ugvouchers.com.peakbw.ugvouchers.db.VoucherDBHelper;
import com.peakbw.ugvouchers.com.peakbw.ugvouchers.net.FTPClientFunctions;

import java.io.File;

public class FTPService extends Service {

    private static final String DEBUG_TAG ="FTPService";
    public static VoucherDBHelper dbHelper;
    private final static String ftpServerHost = "162.144.57.3";
    private final static String ftpServerUsername = "avmmdelivery@katetechglobal.com";
    private final static String ftpServerPassword = "AVMM2015";
    private final static String ftpServerPort = "21";
    private  final  static  String ftpRemoteDirectory = "photos";
    private int count = 0;
    private String srcPath;
    private FTPClientFunctions ftp;

    public FTPService() {
        ftp = new FTPClientFunctions();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        dbHelper = new VoucherDBHelper(this);
        Log.d(DEBUG_TAG, "Service created");
        new SyncTask().execute(ftpServerHost,ftpServerUsername,ftpServerPassword,ftpServerPort);
    }

    public class SyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            Log.d(DEBUG_TAG, "Count = "+count);
            if(count == 4){
                count = 0;
                return  makeConnection(url[0],url[1],url[2],Integer.parseInt(url[3]));
            }else{
                count++;
                return  "Count is "+count;
            }
        }
        @Override
        protected void onPostExecute(String result){
            Log.d(DEBUG_TAG, "Status = " + result);
            if(result!=null && result.equalsIgnoreCase("true")){
                Log.d(DEBUG_TAG, "Status = " + result);
            }
            delay();
        }
    }

    private void delay(){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                new SyncTask().execute(ftpServerHost,ftpServerUsername,ftpServerPassword,ftpServerPort);
            }
        }, 60000);
    }

    private String makeConnection(String host,String username,String password,int port){
        //int respCode = 0;
        String status = null;
        boolean connected,uploaded,disconnected;
        try{
            //get pics to be synced from db
            Cursor cursor = dbHelper.getPics();
            if(cursor.getCount()>0){
                connected = ftp.ftpConnect(host,username,password,port);
                Log.d(DEBUG_TAG,"Connected To FTP Server = "+connected);
                if(connected){
                    String workingDirectory = ftp.ftpGetCurrentWorkingDirectory();
                    Log.d(DEBUG_TAG,"FTP Server Working Directory = "+workingDirectory);
                    String desDirectory = workingDirectory+ftpRemoteDirectory;
                    Log.d(DEBUG_TAG,"FTP Destination Directory = "+desDirectory);

                    while(cursor.moveToNext()){
                        srcPath = cursor.getString(cursor.getColumnIndexOrThrow(VoucherContract.Pics.NAME));
                        Log.d(DEBUG_TAG,"Source Path = "+srcPath);
                        if(srcPath!=null && !srcPath.equals("")){
                            String picName = extractName(srcPath);
                            Log.d(DEBUG_TAG,"Pic Name = "+picName);

                            //create temporary file
                            String tempFile = srcPath.substring(0,srcPath.length()-3)+"temp";
                            Log.d(DEBUG_TAG,"Temporary File = "+tempFile);

                            File original = new File(srcPath);
                            File temp = new File(tempFile);

                            final boolean renameToTemp = original.renameTo(temp);
                            Log.d(DEBUG_TAG,"Rename To Temporary = "+renameToTemp);

                            String tempDesPath = desDirectory+"/"+extractName(tempFile);
                            Log.d(DEBUG_TAG,"Temporary Destination Path = "+tempDesPath);

                            String finalDesPath = desDirectory+"/"+picName;
                            Log.d(DEBUG_TAG,"Final Destination Path = "+finalDesPath);

                            uploaded = ftp.ftpUpload(tempFile,tempDesPath,"",this);
                            Log.d(DEBUG_TAG,"File Uploaded = "+uploaded);

                            boolean renamed = false;

                            if(uploaded){

                                renamed = ftp.ftpRenameFile(tempDesPath,finalDesPath);
                                if(renamed){
                                    updatePicksTable();
                                }
                            }

                            Log.d(DEBUG_TAG, "Renamed to final = " + renamed);

                            final boolean renameToOriginal = temp.renameTo(original);
                            Log.d(DEBUG_TAG,"Rename to Original = "+renameToOriginal);

                            status = String.valueOf(uploaded);

                        }
                    }
                }
                disconnected = ftp.ftpDisconnect();
                Log.d(DEBUG_TAG,"Loged out = "+disconnected);
            }
            cursor.close();

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return  status;
    }

    private String extractName(String path){
        String [] files = path.split("/");
        return  files[files.length-1];
    }

    private void updatePicksTable(){
        //updae pics table
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // New value for one column
        ContentValues values = new ContentValues();
        values.put(VoucherContract.Pics.SYNCED, true);

        // Which row to update, based on the ID
        String selection = VoucherContract.Pics.NAME + " = ?";
        String[] selectionArgs = {srcPath};

        int count = db.update(VoucherContract.Pics.TABLE_NAME,values,selection,selectionArgs);

        //db.close();
        values.clear();
        Log.d(DEBUG_TAG, "Affected Row ID = "+count);
    }
}
