package com.peakbw.ugvouchers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.peakbw.ugvouchers.com.peakbw.ugvouchers.camera.AlbumStorageDirFactory;
import com.peakbw.ugvouchers.com.peakbw.ugvouchers.camera.BaseAlbumDirFactory;
import com.peakbw.ugvouchers.com.peakbw.ugvouchers.camera.FroyoAlbumDirFactory;
import com.peakbw.ugvouchers.com.peakbw.ugvouchers.db.VoucherContract;
import com.peakbw.ugvouchers.com.peakbw.ugvouchers.db.VoucherDBHelper;
import com.peakbw.ugvouchers.com.peakbw.ugvouchers.net.ManageServerConnections;
import com.peakbw.ugvouchers.com.peakbw.ugvouchers.net.XMLParser;
import com.peakbw.ugvouchers.com.peakbw.ugvouchers.services.FTPService;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;


public class DeliveryActivity extends Activity implements LocationListener,GpsStatus.Listener {

    public static final String ACTIVITY_ID = "DeliveryActivity";
    private static final  String url = "http://www.peakbw.com/sandbox/avmmdelivery/backend/delivery/phone_request_handler.php";
    private static final String DEBURG_TAG = "DeliveryActivity";

    private  String mobileNumber;
    private String serialNumber;
    private String voucherNumber;
    private String pin;
    private String imei;
    private String ts;
    private EditText mobTextField,serialTextField,voucherTextField;
    private TextView serialValue,itemValue,voucherValue,serialPreview,itemPreview,voucherPreview;

    private boolean serialCapture = false,itemCaptured = false,voucherCaptured = false;

    private static final int ACTION_CAPTURE_SN = 1;
    private static final int ACTION_CAPTURE_ITEM = 2;
    private static final int ACTION_CAPTURE_VOUCHER = 3;

    private static final String NUMBER_STORAGE_KEY = "consent";
    private static final String SN_STORAGE_KEY = "photo";
    private static final String VOUCHER_STORAGE_KEY = "id";
    private static final String PATH_STORAGE_KEY = "path";

    private static final String ITEM_KEY = "m";
    private static final String SERIAL_KEY = "i";
    private static final String VOUCHER_KEY = "v";

    private static final String ITEM_PIC_KEY = "ip";
    private static final String SERIAL_PIC_KEY = "sp";
    private static final String VOUCHER_PIC_KEY = "vp";
    private static final String PICS_ARRAY_KEY = "pak";
    //private static final String ALERT_MSG_KEY = "msg";

    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private String photoName,item,serial,voucher;
    private String snFile= "",voucherFile= "",itemFile="";
    private String currentFilePath;
    private static final String JPEG_FILE_SUFFIX = ".jpg";

    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;

    private LocationManager locationManager;
    private static final int TWO_MINUTES = 1000 * 60 * 2;
    private Location currentBestLocation = null;
    private double lat,lng,latitude,longitude;
    private float accuracy,acc;
    private int satellites = 0,sate;

    public static boolean refreshDisplay = true;

    public static VoucherDBHelper rDbHelper;
    private SQLiteDatabase db;

    private ManageServerConnections mdc;

    //public static boolean wifiConnected = false;
    //public static boolean mobileConnected = false;

    private static ContentValues values = new ContentValues();

    private static String filePrefix;

    private String [] picsArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);

        // get data from previous activity
        Intent i = this.getIntent();
        pin = i.getStringExtra("pin");
        imei = i.getStringExtra("imei");

        mdc = new ManageServerConnections();

        Log.d(ACTIVITY_ID, "PIN = " + pin);

        // initialize layout text fields
        mobTextField = (EditText)findViewById(R.id.mob);
        mobTextField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.ok || id == EditorInfo.IME_NULL) {
                    validate(R.id.ok);
                    return true;
                }
                return false;
            }
        });

        serialTextField = (EditText)findViewById(R.id.sn);
        serialTextField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.ok || id == EditorInfo.IME_NULL) {
                    validate(R.id.ok);
                    return true;
                }
                return false;
            }
        });

        voucherTextField = (EditText)findViewById(R.id.voucher);
        voucherTextField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id,KeyEvent keyEvent) {
                if (id == R.id.ok || id == EditorInfo.IME_NULL) {
                    validate(R.id.ok);
                    return true;
                }
                return false;
            }
        });

        itemValue = (TextView)findViewById(R.id.itemValue);
        serialValue = (TextView)findViewById(R.id.serialValue);
        voucherValue = (TextView)findViewById(R.id.voucherValue);

        itemPreview = (TextView)findViewById(R.id.itemPreview);
        serialPreview = (TextView)findViewById(R.id.snPreview);
        voucherPreview = (TextView)findViewById(R.id.voucherPreview);

        //create or open sqlite database
        rDbHelper = new VoucherDBHelper(getBaseContext());
        db = rDbHelper.getWritableDatabase();

        //get user set server preferences
        //SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        //url = sharedPrefs.getString("server_url", "");

        picsArray = new String[3];

        Intent serviceIntent = new Intent(this, FTPService.class);
        startService(serviceIntent);

        //ActionBar actionBar = getActionBar();
        //actionBar.setCustomView(R.layout.top_bar_menu); //load your layout
        //actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME|ActionBar.DISPLAY_SHOW_CUSTOM); //show it
    }


    @Override
    public void onStart(){
        super.onStart();

        filePrefix = "";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }

        updateLocation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.delivery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_history) {
            Intent intent = new Intent(this,History.class);
            startActivity(intent);
            return true;
        }
        else if(id == R.id.action_settings){
            Intent settingsActivity = new Intent(getBaseContext(), SettingsActivity.class);
            startActivity(settingsActivity);
            return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_DOWNLOAD_PROGRESS:
                ProgressDialog mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("please wait");
                mProgressDialog.setTitle("Delivery Manager");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgressDialog.setCancelable(true);
                //mProgressDialog.show();

                return mProgressDialog;

            default:
                return null;
        }
    }

    public void actionListener(View view){
        int viewId = view.getId();

        if(viewId == R.id.cancel){
            finish();
        }else{
            validate(viewId);
        }
    }

    private void validate(int viewID){

        Log.d(DEBURG_TAG, "Clicked View = " + viewID);

        mobTextField.setError(null);
        serialTextField.setError(null);
        voucherTextField.setError(null);

        mobileNumber = mobTextField.getText().toString();
        filePrefix = mobileNumber;

        String networkCode = "";

        if(mobileNumber.length() == 10){
            networkCode = mobileNumber.substring(0,3);
        }

        serialNumber = serialTextField.getText().toString();
        voucherNumber = voucherTextField.getText().toString();


        boolean cancel = false;
        View focusView = null;

        //validate mobile number field
        if (TextUtils.isEmpty(mobileNumber)) {
            mobTextField.setError(getString(R.string.error_mob_required));
            focusView = mobTextField;
            cancel = true;
        } else if (mobileNumber.length() < 10) {
            mobTextField.setError(getString(R.string.error_invalid_mob));
            focusView = mobTextField;
            cancel = true;
        }else if(networkCode.length()==3 && !(networkCode.equals("077")||networkCode.equals("078")||networkCode.equals("075")||networkCode.equals("070")||networkCode.equals("071")||networkCode.equals("079"))){
            mobTextField.setError(getString(R.string.error_invalid_mob));
            focusView = mobTextField;
            cancel = true;
        }

        //validate serial Number field
        if (TextUtils.isEmpty(serialNumber)) {
            serialTextField.setError(getString(R.string.error_sn_required));
            focusView = serialTextField;
            cancel = true;
        } else if (serialNumber.length() < 1) {
            serialTextField.setError(getString(R.string.error_invalid_sn));
            focusView = serialTextField;
            cancel = true;
        }

        //validate voucher number
        if (TextUtils.isEmpty(voucherNumber)) {
            voucherTextField.setError(getString(R.string.error_voucher_required));
            focusView = voucherTextField;
            cancel = true;
        } else if (voucherNumber.length() < 1) {
            voucherTextField.setError(getString(R.string.error_invalid_voc));
            focusView = voucherTextField;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt continue and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            if(viewID == R.id.ok){

                if(serialCapture){
                    if(itemCaptured){
                        if(voucherCaptured){

                            serialCapture = false;
                            itemCaptured = false;
                            voucherCaptured = false;

                            serialValue.setText(getString(R.string.value));
                            itemValue.setText(R.string.value);
                            voucherValue.setText(R.string.value);

                            serialPreview.setVisibility(View.INVISIBLE);
                            itemPreview.setVisibility(View.INVISIBLE);
                            voucherPreview.setVisibility(View.INVISIBLE);

                            String request = createXML();
                            mdc.setUpdateVariable(request);
                            new ConnectionTask().execute(url);

                            mobTextField.setText("");
                            serialTextField.setText("");
                            voucherTextField.setText("");

                        }else{
                            createAlertDialog("Voucher not Captured.\nDo you want to capture it?", R.layout.error_prompt, R.id.error);
                        }
                    }else{
                        createAlertDialog("Item not Captured.\nDo you want to capture it?", R.layout.error_prompt, R.id.error);
                    }
                }else{
                    createAlertDialog("Serial Number not Captured.\nDo you want to capture it?",R.layout.error_prompt,R.id.error);
                }

            }else if(viewID == R.id.serialLabel){
                serial = dispatchTakePictureIntent(ACTION_CAPTURE_SN);
                Log.d(DEBURG_TAG, "snFile = "+serial);
            }else if(viewID == R.id.itemLabel){
                item = dispatchTakePictureIntent(ACTION_CAPTURE_ITEM);
                Log.d(DEBURG_TAG, "itemFile = "+item);
            }else if(viewID == R.id.voucherLabel){
                voucher = dispatchTakePictureIntent(ACTION_CAPTURE_VOUCHER);
                Log.d(DEBURG_TAG, "voucherFile = "+voucher);
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        try{
            Location lctn;
            if(isBetterLocation(location, currentBestLocation)){
                lctn = location;
            }
            else{
                lctn = currentBestLocation;
            }
            lat = lctn.getLatitude();
            lng = lctn.getLongitude();
            accuracy = lctn.getAccuracy();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void updateLocation(){

        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled && !isNetworkEnabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
        else{
            //String provider;
            if (isNetworkEnabled) {
                //provider = LocationManager.NETWORK_PROVIDER;
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,this);
            }

            if (isGPSEnabled) {
                ///provider = LocationManager.GPS_PROVIDER;
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,this);
            }
        }
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        try{
            if (currentBestLocation == null) {
                // A new location is always better than no location
                return true;
            }

            // Check whether the new location fix is newer or older
            long timeDelta = location.getTime() - currentBestLocation.getTime();
            boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
            boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
            boolean isNewer = timeDelta > 0;

            // If it's been more than two minutes since the current location, use the new location
            // because the user has likely moved
            if (isSignificantlyNewer) {
                return true;
                // If the new location is more than two minutes older, it must be worse
            } else if (isSignificantlyOlder) {
                return false;
            }

            // Check whether the new location fix is more or less accurate
            int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
            boolean isLessAccurate = accuracyDelta > 0;
            boolean isMoreAccurate = accuracyDelta < 0;
            boolean isSignificantlyLessAccurate = accuracyDelta > 200;

            // Check if the old and new location are from the same provider
            boolean isFromSameProvider = isSameProvider(location.getProvider(),
                    currentBestLocation.getProvider());

            // Determine location quality using a combination of timeliness and accuracy
            if (isMoreAccurate) {
                return true;
            } else if (isNewer && !isLessAccurate) {
                return true;
            } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
                return true;
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        try{
            if (provider1 == null) {
                return provider2 == null;
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return provider1.equals(provider2);
    }

    @Override
    public void onGpsStatusChanged(int event) {
        int satellitesInFix = 0;
        int timetofix = locationManager.getGpsStatus(null).getTimeToFirstFix();
        Log.i(ACTIVITY_ID, "Time to first fix = " + String.valueOf(timetofix));
        for (GpsSatellite sat : locationManager.getGpsStatus(null).getSatellites()) {
            if(sat.usedInFix()) {
                satellitesInFix++;
            }
            satellites++;
        }
        Log.i(ACTIVITY_ID, String.valueOf(satellites) + " Used In Last Fix (" + satellitesInFix + ")");
    }

    /*private void updateConnectedFlags() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeInfo = connMgr.getActiveNetworkInfo();
        if (activeInfo != null && activeInfo.isConnected()) {
            wifiConnected = activeInfo.getType() == ConnectivityManager.TYPE_WIFI;
            mobileConnected = activeInfo.getType() == ConnectivityManager.TYPE_MOBILE;
        } else {
            wifiConnected = false;
            mobileConnected = false;
        }

        Log.d(ACTIVITY_ID, "connected to wifi = "+wifiConnected);
        Log.d(ACTIVITY_ID, "connected to mobile = "+mobileConnected);

    }*/

    private String createXML(){
        StringBuilder xmlStr = new StringBuilder();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<pbtRequest>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<method>").append("pbt_voucher_delivery").append("</method>");
        xmlStr.append("<imei>").append(imei).append("</imei>");
        xmlStr.append("<msisdn>").append(mobileNumber.replaceFirst("0","256")).append("</msisdn>");
        xmlStr.append("<voucherSN>").append(voucherNumber).append("</voucherSN>");
        xmlStr.append("<deliverySN>").append(serialNumber).append("</deliverySN>");

        xmlStr.append("<itemPic>").append(item).append("</itemPic>");
        xmlStr.append("<voucherPic>").append(voucher).append("</voucherPic>");
        xmlStr.append("<serialPic>").append(serial).append("</serialPic>");

        latitude = lat;
        longitude = lng;
        xmlStr.append("<gpsLong>").append(longitude).append("</gpsLong>");
        xmlStr.append("<gpsLat>").append(latitude).append("</gpsLat>");
        sate = satellites;
        acc = accuracy;
        xmlStr.append("<accuracy>").append(acc).append("</accuracy>");
        xmlStr.append("<numSatelites>").append(sate).append("</numSatelites>");
        ts = getDateTime();
        xmlStr.append("<deliveryTS>").append(ts).append("</deliveryTS>");
        xmlStr.append("</pbtRequest>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }

    public static String getDateTime() {

        //Date sqlDateTime = new Date(System.currentTimeMillis());

        String mnt = "";
        String [] months = {"Jan","Feb","Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"};
        String [] dateInfo = ((new Date()).toString()).split(" ");//Fri Nov 29 11:57:35 EAT 2013
        if(dateInfo[1].equalsIgnoreCase(months[0])){
            mnt = "01";
        }
        else if(dateInfo[1].equalsIgnoreCase(months[1])){
            mnt = "02";
        }else if(dateInfo[1].equalsIgnoreCase(months[2])){
            mnt = "03";
        }else if(dateInfo[1].equalsIgnoreCase(months[3])){
            mnt = "04";
        }else if(dateInfo[1].equalsIgnoreCase(months[4])){
            mnt = "05";
        }else if(dateInfo[1].equalsIgnoreCase(months[5])){
            mnt = "06";
        }else if(dateInfo[1].equalsIgnoreCase(months[6])){
            mnt = "07";
        }else if(dateInfo[1].equalsIgnoreCase(months[7])){
            mnt = "08";
        }else if(dateInfo[1].equalsIgnoreCase(months[8])){
            mnt = "09";
        }else if(dateInfo[1].equalsIgnoreCase(months[9])){
            mnt = "10";
        }else if(dateInfo[1].equalsIgnoreCase(months[10])){
            mnt = "11";
        }else if(dateInfo[1].equalsIgnoreCase(months[11])){
            mnt = "12";
        }

        return dateInfo[5]+"-"+mnt+"-"+dateInfo[2]+" "+dateInfo[3];
    }

    public class ConnectionTask extends AsyncTask<String, String, String> {
        private String status = "";
        private InputStream stream = null;
        private static final String DEBUG_TAG = "DownloadXMLTask";

        //ByteArrayOutputStream baos = new ByteArrayOutputStream();

        @SuppressWarnings("deprecation")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(DIALOG_DOWNLOAD_PROGRESS);
        }

        @Override
        protected String doInBackground(String... url) {
            try {
                stream = mdc.downloadUrl(url[0]);
                if(stream!=null){
                    try	{
                        status = XMLParser.parseXML(stream);
                        stream.close();
                    }
                    catch(Exception ex){
                        ex.printStackTrace();
                    }
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
            finally {
                if (stream != null) {
                    try{
                        stream.close();
                    }catch(IOException ex){
                        ex.printStackTrace();
                    }
                }
            }
            return status;
        }

        @Override
        protected void onPostExecute(String result) {
            dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
            Log.d(DEBUG_TAG, "result = "+result);
            if(result!=null){
                if(result.contains("SUCCESSFUL")){//
                    createResponseDialog(result.substring(10,result.length()-1),R.layout.success_prompt,R.id.success);
                }
                else{
                    if(result.equals("")){
                        createResponseDialog("Invalid Response",R.layout.error_prompt,R.id.error);
                    }
                    else{
                        createResponseDialog(result,R.layout.error_prompt,R.id.error);
                    }
                }
            }
            insertHistory(db,result);
        }
    }

    private void createResponseDialog(String msg,int layout,int textView){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(layout, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        final TextView msgTextView = (TextView) promptsView.findViewById(textView);
        msgTextView.setText(msg);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //onStart();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void createAlertDialog(String msg,int layout,int textView){
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(layout, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);

        final TextView msgTextView = (TextView) promptsView.findViewById(textView);
        msgTextView.setText(msg);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            if (!serialCapture) {
                serial = dispatchTakePictureIntent(ACTION_CAPTURE_SN);
                Log.d(DEBURG_TAG, "snFile = " + serial);
            } else if (!itemCaptured) {
                item = dispatchTakePictureIntent(ACTION_CAPTURE_ITEM);
                Log.d(DEBURG_TAG, "itemFile = " + item);
            } else if (!voucherCaptured) {
                voucher = dispatchTakePictureIntent(ACTION_CAPTURE_VOUCHER);
                Log.d(DEBURG_TAG, "voucherFile = " + voucher);
            }
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void createImagePreviewDialog(Uri imageUri){
        LayoutInflater li = LayoutInflater.from(this);

        View imageView = li.inflate(R.layout.image_preview, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(imageView);

        final ImageView view = (ImageView) imageView.findViewById(R.id.image_preview);
        view.setImageURI(imageUri);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //onStart();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void insertHistory(SQLiteDatabase db,String status){
        values.put(VoucherContract.History.MSISDN, mobileNumber);
        values.put(VoucherContract.History.DELIVERY_NUMBER, serialNumber);
        values.put(VoucherContract.History.VOUCHER_NUMBER, voucherNumber);

        values.put(VoucherContract.History.SERIAL_PIC, snFile);
        values.put(VoucherContract.History.ITEM_PIC, itemFile);
        values.put(VoucherContract.History.VOUCHER_PIC, voucherFile);

        values.put(VoucherContract.History.TS, ts);
        values.put(VoucherContract.History.GPS_LAT, latitude);
        values.put(VoucherContract.History.GPS_LONG, longitude);
        values.put(VoucherContract.History.NUMBER_OF_SATELITES, sate);
        values.put(VoucherContract.History.ACCURACY, acc);
        values.put(VoucherContract.History.STATUS, status);

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insertWithOnConflict(VoucherContract.History.TABLE_NAME,VoucherContract.History.COLUMN_NAME_NULLABLE,values,SQLiteDatabase.CONFLICT_REPLACE);

        Log.d(ACTIVITY_ID, "New Transaction Row Id:"+ newRowId+" "+"0");
        Log.d(ACTIVITY_ID, "Transaction Status: " + status);
        values.clear();

        //insert picture absolute paths into pics table
        for (String aPicsArray : picsArray) {
            Log.d(ACTIVITY_ID, "PIC = "+aPicsArray);
            values.put(VoucherContract.Pics.MSISDN, mobileNumber);
            values.put(VoucherContract.Pics.NAME, aPicsArray);
            values.put(VoucherContract.Pics.SYNCED, false);
            db.insert(VoucherContract.Pics.TABLE_NAME, VoucherContract.Pics.COLUMN_NAME_NULLABLE, values);
            values.clear();
        }
    }

    public void itemClickListener(View view){
        int id = view.getId();
        Log.d(DEBURG_TAG, String.valueOf(id));

        if(id == R.id.snPreview){
            File f = new File(picsArray[1]);
            Uri imageUri = Uri.fromFile(f);
            createImagePreviewDialog(imageUri);
        }else if(id == R.id.itemPreview){
            File f = new File(picsArray[0]);
            Uri imageUri = Uri.fromFile(f);
            createImagePreviewDialog(imageUri);
        }else if(id == R.id.voucherPreview){
            File f = new File(picsArray[2]);
            Uri imageUri = Uri.fromFile(f);
            createImagePreviewDialog(imageUri);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        String path = currentFilePath;

        Log.v("AddToGallery", "RequestCode = " + requestCode);

        if(requestCode == ACTION_CAPTURE_ITEM){
            itemCaptured = true;
            itemPreview.setVisibility(View.VISIBLE);
            itemValue.setText(getString(R.string.true_text));

            itemFile = path;
            picsArray[0] = itemFile;
            item = extractName(path);
            Log.v("AddToGallery", "itemPic = "+item);
        }
        else if(requestCode == ACTION_CAPTURE_SN){
            serialCapture = true;
            serialPreview.setVisibility(View.VISIBLE);
            serialValue.setText(getString(R.string.true_text));

            snFile = path;
            picsArray[1] = snFile;
            serial = extractName(path);
            Log.d("AddToGallery", "SerilaPic = "+serial);
        }
        else if(requestCode == ACTION_CAPTURE_VOUCHER){
            voucherCaptured = true;
            voucherPreview.setVisibility(View.VISIBLE);
            voucherValue.setText(getString(R.string.true_text));

            voucherFile = path;
            picsArray[2] =  voucherFile;
            voucher = extractName(path);
            Log.d("AddToGallery", "VoucherPic = "+voucher);
        }

        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);

        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);

    }

    @Override
    // save activity state
    protected void onSaveInstanceState(Bundle outState) {

        outState.putString(NUMBER_STORAGE_KEY, mobileNumber);
        outState.putString(SN_STORAGE_KEY, serialNumber);
        outState.putString(VOUCHER_STORAGE_KEY, voucherNumber);

        outState.putString(PATH_STORAGE_KEY, currentFilePath);

        outState.putBoolean(ITEM_KEY, itemCaptured);
        outState.putBoolean(SERIAL_KEY, serialCapture);
        outState.putBoolean(VOUCHER_KEY, voucherCaptured);

        outState.putString(ITEM_PIC_KEY, item);
        outState.putString(SERIAL_PIC_KEY, serial);
        outState.putString(VOUCHER_PIC_KEY, voucher);

        //outState.putString(ALERT_MSG_KEY, alertMessage);

        outState.putStringArray(PICS_ARRAY_KEY, picsArray);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mobTextField.setText(savedInstanceState.getString(NUMBER_STORAGE_KEY));
        serialTextField.setText(savedInstanceState.getString(SN_STORAGE_KEY));
        voucherTextField.setText(savedInstanceState.getString(VOUCHER_STORAGE_KEY));

        currentFilePath = savedInstanceState.getString(PATH_STORAGE_KEY);

        item = savedInstanceState.getString(ITEM_PIC_KEY);
        serial = savedInstanceState.getString(SERIAL_PIC_KEY);
        voucher = savedInstanceState.getString(VOUCHER_PIC_KEY);

        picsArray = savedInstanceState.getStringArray(PICS_ARRAY_KEY);

        itemCaptured = savedInstanceState.getBoolean(ITEM_KEY);
        serialCapture = savedInstanceState.getBoolean(SERIAL_KEY);
        voucherCaptured = savedInstanceState.getBoolean(VOUCHER_KEY);

        //alertMessage = savedInstanceState.getString(ALERT_MSG_KEY);

        if(itemCaptured){
            itemPreview.setVisibility(View.VISIBLE);
            itemValue.setText(getString(R.string.true_text));
        }

        if(serialCapture){
            serialPreview.setVisibility(View.VISIBLE);
            serialValue.setText(getString(R.string.true_text));
        }

        if( voucherCaptured){
            voucherPreview.setVisibility(View.VISIBLE);
            voucherValue.setText(getString(R.string.true_text));
        }

    }

    private String dispatchTakePictureIntent(int actionCode) {
        String fileName ="";
        switch(actionCode) {
            case ACTION_CAPTURE_SN:
                photoName = filePrefix+"_serialNumber_";
                fileName = startCameraActivity(actionCode);
                Log.d(DEBURG_TAG, "S/N = "+fileName);
                break;

            case ACTION_CAPTURE_ITEM:
                photoName = filePrefix+"_item_";
                fileName = startCameraActivity(actionCode);
                Log.d(DEBURG_TAG, "Item = "+fileName);
                break;

            case ACTION_CAPTURE_VOUCHER:
                photoName = filePrefix+"_voucher_";
                fileName = startCameraActivity(actionCode);
                Log.d(DEBURG_TAG, "Voucher = "+fileName);
                break;

            default:
                break;
        }
        return fileName;
    }

    private String startCameraActivity(int actionCode){
        String fileName ="";
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f;
        try {
            f = setUpPhotoFile();
            fileName = f.getName();
            Log.d("startCameraActivity", "takenPhoto = "+fileName);
            currentFilePath = f.getAbsolutePath();
            Log.d(DEBURG_TAG, "PATH  = "+currentFilePath);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            startActivityForResult(takePictureIntent, actionCode);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return fileName;
    }


    private File setUpPhotoFile() throws IOException {

        File f = createImageFile();
        String file = f.getName();
        Log.d(DEBURG_TAG, "RealFileName = "+file);

        return f;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = photoName;
        Log.d(DEBURG_TAG, "ImageName = "+imageFileName);
        File albumF = getAlbumDir();
        return File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
    }

    private File getAlbumDir() {
        File storageDir = null;
        try{
            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

                storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());

                if (storageDir != null) {
                    if (! storageDir.mkdirs()) {
                        if (! storageDir.exists()){
                            Log.d("CameraSample", "failed to create directory");
                            return null;
                        }
                    }
                }

            }
            else {
                Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }

        return storageDir;
    }

    private String getAlbumName() {

        return getString(R.string.album_name);
    }

    /*private String getPath(Uri uri){
        String[] filePathColumn = { MediaStore.Images.Media.DATA };

        Cursor cursor = getContentResolver().query(uri,filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();

        return picturePath;
    }*/

    private String extractName(String path){
        Log.d(DEBURG_TAG,"File Abs Path = "+path);
        String [] files = path.split("/");
        return  files[files.length-1];
    }
}
