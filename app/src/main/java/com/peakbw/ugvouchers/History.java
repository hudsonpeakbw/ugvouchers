package com.peakbw.ugvouchers;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.peakbw.ugvouchers.com.peakbw.ugvouchers.db.VoucherContract;
import com.peakbw.ugvouchers.com.peakbw.ugvouchers.db.VoucherDBHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;


public class History extends ListActivity {

    private VoucherDBHelper rDbHelper;
    private SimpleAdapter adapter;
    private static final String ACTIVITY_ID = "History";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        setTitle("History");

        rDbHelper = new VoucherDBHelper(getBaseContext());
    }

    @Override
    protected void onStart() {
        super.onStart();

        try{
            Cursor cursor = rDbHelper.getHistory();

            //ArrayList<String> historyList = new ArrayList<String>();
            List<Map<String, String>> historyList = new ArrayList<Map<String, String>>();

            while(cursor.moveToNext()){
                Map<String, String> datum = new HashMap<String, String>(2);
                String msisdn = cursor.getString(cursor.getColumnIndexOrThrow(VoucherContract.History.MSISDN));
                String id = cursor.getString(cursor.getColumnIndexOrThrow(VoucherContract.History._ID));
                String ts = cursor.getString(cursor.getColumnIndexOrThrow(VoucherContract.History.TS));

                String trxn = ts+"->"+msisdn;

                datum.put("element", trxn);

                historyList.add(datum);
            }

            cursor.close();

            adapter = new SimpleAdapter(this, historyList,R.layout.list_elements,new String[] {"element"},new int[] {R.id.item});
            setListAdapter(adapter);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        HashMap<String, String> histMap = (HashMap<String, String>)l.getItemAtPosition(position);
        String selectedHist = histMap.get("element");
        Log.d(ACTIVITY_ID, "Selected Item " + selectedHist);

        Intent intent = new Intent(this,Details.class);
        intent.putExtra("selected",selectedHist);
        startActivity(intent);
    }
}
